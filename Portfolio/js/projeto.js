new WOW().init();

TweenMax.staggerFrom("li, a", 1,{
    delay:0.35,
    opacity:0,
    ease: Expo.easeInOut
},0.07);

TweenMax.staggerFrom(".desc-btn", 1,{
    delay:1,
    opacity:0,
    ease: Expo.easeInOut
},0.07);

var owl = $('.owl-carousel');

$(document).ready(function(){
 owl.owlCarousel();

});

owl.owlCarousel({
    loop:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:2000,
    autoplayHoverPause:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },

        600:{
            items:2,
            nav:true
          
        }
    }
})
$('.play').on('click',function(){
    owl.trigger('play.owl.autoplay',[1500])
})
$('.stop').on('click',function(){
    owl.trigger('stop.owl.autoplay')
})

var btnMenu = document.querySelector(".btn-menu");
var conjLi = document.querySelector(".conj-li");
var itemSobre = document.querySelector(".item-sobre");
var itemTrabs = document.querySelector(".item-trabs");
var itemContacts = document.querySelector(".item-contacts");


btnMenu.addEventListener("click", () => {
	
	conjLi.classList.toggle("click");


});

itemSobre.addEventListener("click", () => {
    
    conjLi.classList.toggle("click");


});

itemTrabs.addEventListener("click", () => {
    
    conjLi.classList.toggle("click");


});

itemContacts.addEventListener("click", () => {
    
    conjLi.classList.toggle("click");


});
